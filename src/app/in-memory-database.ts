import { InMemoryDbService } from  "angular-in-memory-web-api";

import { Category } from "./pages/categories/shared/category.model";
import { Entry } from './pages/entries/shared/entry.model';

export class InMemoryDatabase implements InMemoryDbService{
    createDb() {
        const categories: Category[] = [
            {id: 1, name: 'Moradia', description: 'Pagamentos de Contas de Casa'},
            {id: 2, name: 'Saúde', description: 'Plano de Saúde e Remédios'},
            {id: 3, name: 'Lazer', description: 'Cinema, parques, praia, etc.'},
            {id: 4, name: 'Salário', description: 'Recebimento de Salário'},
            {id: 5, name: 'Necessidades', description: 'Necessidades mansais'},
        ];

        const entries: Entry[] = [
          { id: 1, name: 'Gás de Cozinha', categoryId: categories[0].id, category: categories[0], paid: true, date: '26/10/2019', amount: '70,80', type: 'expense', description: 'Pagamentos de Casa' } as Entry,
          { id: 2, name: 'Salaŕio', categoryId: categories[3].id, category: categories[3], paid: true, date: '15/01/2020', amount: '1600', type: 'revenue' } as Entry,
          { id: 3, name: 'Uber', categoryId: categories[4].id, category: categories[4], paid: false, date: '30/01/2020', amount: '254,56', type: 'expense', description: 'Mobilidades' } as Entry,
          { id: 4, name: 'Restaurantes', categoryId: categories[2].id, category: categories[2], paid: false, date: '30/01/2020', amount: '500', type: 'expense' } as Entry
        ];

        return { categories, entries };
    }
}